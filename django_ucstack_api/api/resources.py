from django.forms import model_to_dict
from tastypie.exceptions import Unauthorized, BadRequest
from tastypie.resources import Resource
from tastypie.serializers import Serializer

from django_directory_backend_client.django_directory_backend_client.func import *
from django_tastypie_generalized_api_auth.django_tastypie_generalized_api_auth.authentification import (
    ApiAuthCustomerAuthentication,
)
from django_tastypie_generalized_api_auth.django_tastypie_generalized_api_auth.models import (
    ApiAuthCustomerApiKey,
)
from django_ucstack_mailstore_spe_client.django_ucstack_mailstore_spe_client.func import (
    post_to_mailstore_spe_api,
)
from django_ucstack_mailstore_spe_models.django_ucstack_mailstore_spe_models.models import (
    UcMailstoreUsage,
)
from django_ucstack_models.django_ucstack_models.models import *
from django_ucstack_powershell_scripts.django_ucstack_powershell_scripts.func import *


def check_permission(tenant: str, customer_id: int):
    user_api_auth = ApiAuthCustomerApiKey.objects.get(user_id=customer_id)

    if user_api_auth.super_admin:
        return True

    return False


class UcApiAuthCustomerApiKeyResource(Resource):
    class Meta:
        queryset = ApiAuthCustomerApiKey.objects.all()
        limit = 0
        max_limit = 0
        always_return_data = True
        allowed_methods = ["get", "post"]
        serializer = Serializer()
        authentication = ApiAuthCustomerAuthentication()

    def get_resource_uri(self, bundle_or_obj=None, url_name="api_dispatch_list"):
        return

    def obj_get(self, bundle, **kwargs):
        if bundle.request.user.auth_api_key.super_admin == True:
            bundle = ApiAuthCustomerApiKey.objects.get(user__id=str(kwargs["pk"]))
        else:
            raise Unauthorized
        return bundle

    def obj_create(self, bundle, **kwargs):
        if bundle.request.user.auth_api_key.super_admin == True:
            api_key = ApiAuthCustomerApiKey(user=bundle.request.user)
        else:
            raise Unauthorized

        api_key.save()
        bundle.obj = api_key

        return bundle

    def dehydrate(self, bundle):
        bundle.data = dict()

        bundle.data = model_to_dict(bundle.obj)
        bundle.data["user"] = model_to_dict(bundle.obj.user)

        return bundle


class UcBeBaseResource(Resource):
    class Meta:
        queryset = None
        always_return_data = True
        allowed_methods = ["get", "post"]
        serializer = Serializer()
        authentication = ApiAuthCustomerAuthentication()
        limit = 0
        max_limit = 0


class MicrosoftExchangeAdminUserRightsPsResource(UcBeBaseResource):
    class Meta(UcBeBaseResource.Meta):
        queryset = MdatCustomers.objects.all()

    def get_resource_uri(self, bundle_or_obj=None, url_name="api_dispatch_list"):
        return

    def obj_get_list(self, bundle, **kwargs):
        if not check_permission("", bundle.request.user.id):
            raise BadRequest("Wrong tenant.")

        object_list = list()

        tenants = directory_backend_get_tenants(settings.MDAT_ROOT_CUSTOMER_ID)

        for tenant in tenants:
            try:
                tenant_uc_settings = UcTenantSettings.objects.get(
                    org_tag=tenant["companyCode"]
                )
            except UcTenantSettings.DoesNotExist:
                tenant_uc_settings = UcTenantSettings(
                    org_tag=tenant["companyCode"],
                    customer=MdatCustomers.objects.get(org_tag=tenant["companyCode"]),
                )
                tenant_uc_settings.save()

            tenant_uc_settings.refresh_from_db()

            if tenant_uc_settings.msexchange_enabled:
                data = dict()
                data["org_tag"] = tenant["companyCode"]
                data["script"] = render_microsoft_exchange_admin_user_rights(
                    data["org_tag"],
                    settings.HOSTING_LDAP_SEARCH_BASE,
                    tenant["super_admin_dn"],
                )

                object_list.append(data)

        return object_list

    def dehydrate(self, bundle):
        bundle.data = bundle.obj
        return bundle


class MicrosoftExchangeFullAccessEverybodyPsResource(UcBeBaseResource):
    class Meta(UcBeBaseResource.Meta):
        queryset = MdatCustomers.objects.all()

    def get_resource_uri(self, bundle_or_obj=None, url_name="api_dispatch_list"):
        return

    def obj_get_list(self, bundle, **kwargs):
        if not check_permission("", bundle.request.user.id):
            raise BadRequest("Wrong tenant.")

        object_list = list()

        tenants = directory_backend_get_tenants(settings.MDAT_ROOT_CUSTOMER_ID)

        for tenant in tenants:
            try:
                tenant_uc_settings = UcTenantSettings.objects.get(
                    org_tag=tenant["companyCode"]
                )
            except UcTenantSettings.DoesNotExist:
                tenant_uc_settings = UcTenantSettings(
                    org_tag=tenant["companyCode"],
                    customer=MdatCustomers.objects.get(org_tag=tenant["companyCode"]),
                )
                tenant_uc_settings.save()

            tenant_uc_settings.refresh_from_db()

            if (
                tenant_uc_settings.msexchange_enabled
                and tenant_uc_settings.full_access_eb
            ):
                data = dict()
                data["org_tag"] = tenant["companyCode"]
                data["script"] = render_microsoft_exchange_full_access_everybody(
                    data["org_tag"],
                    settings.HOSTING_LDAP_SEARCH_BASE,
                )

                object_list.append(data)

        return object_list

    def dehydrate(self, bundle):
        bundle.data = bundle.obj
        return bundle


class UcCustomerMailarchiveLicenseResource(UcBeBaseResource):
    class Meta(UcBeBaseResource.Meta):
        queryset = None

    def get_resource_uri(self, bundle_or_obj=None, url_name="api_dispatch_list"):
        return

    def obj_get_list(self, bundle, **kwargs):
        if bundle.request.user.auth_api_key.super_admin is False:
            raise Unauthorized

        return UcMailstoreUsage.objects.all()

    def dehydrate(self, bundle):
        bundle.data = model_to_dict(bundle.obj)
        bundle.data["licenses_needed"] = bundle.obj.licenses_needed
        bundle.data["mailstore_id"] = (
            bundle.obj.customer.uctenantsettings_set.first().mailstore_id
        )
        return bundle
